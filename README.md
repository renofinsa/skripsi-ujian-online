## About Laravel
SISTEM UJIAN ONLINE

## Requirements
- PHP 7.4++
- MySQL 5++
- Composer
- Laravel 6.0

## Install
- Open terminal (Mac Os) / command prompt (Windows)
- Clone project "git clone https://gitlab.com/renofinsa/skripsi-ujian-onine.git"
- copy and paste .env.example and rename the file to .env, or you can write it like this "cp .env.example .env"
- setup your database, DB_DATABASE, DB_USERNAME, DB_PASSWORD
- next step "php artisan key:generate"
- migrate your database "php artisan migrate"
- get your database "php atisan db:seed"
- To run application you can write this script "php artisan serv"
- Open your browser and call URL (localhost:8000) default port is 8000

## ---------------------------------------------------------------------------------------------

## Actors
- Admin
- Guru
- Siswa

## Features
**Admin**
- Beranda
- Data Guru
- Data Siswa
- Kelas
- Nilai
- Jenis Ujian

**Guru**
- Beranda
- Pengaturan Ujian
    - Buat Ujian
    - Data Nilai
    - Tambah Soal
- Data Siswa

**Siswa**
- Beranda
- Data Nilai

## Application Flow
**Step 1 | Preparing Data | Admin**
- Create Guru
- Create Kelas
- Create Jenis Ujian
- Create Siswa

**Step 2 | Buat Soal | Guru**
- Create Ujian 
- Buat Soal

**Step 3 | Ujian |Tutor - Siswa**
- Pilih Ujian
- Mengikuti Ujian
- Mendapatkan Nilai
