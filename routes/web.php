<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// logout
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');

Route::get('/404', function () {
    return view('404');
})->name('404');

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

// excel import
Route::post('/import-teacher', 'ExcelController@uploadDataTeacher')->name('import-teacher');
Route::post('/import-student', 'ExcelController@uploadDataStudent')->name('import-student');
Route::post('/import-question', 'ExcelController@uploadDataQuestion')->name('import-question');

// excel export
Route::get('/export-teacher', 'ExcelController@DownloadDataTeacher')->name('export-teacher');
Route::get('/export-student', 'ExcelController@DownloadDataStudent')->name('export-student');
Route::get('/export-score', 'ExcelController@downloadDataScore')->name('export-score');
Route::get('/export-score/{id}', 'ExcelController@downloadDataScoreDetail')->name('export-score-detail');

Route::get('/home', 'HomeController@index')->name('home');

// admin change password
Route::get('/change-password', 'HomeController@changePassword')->name('change-password');
Route::post('/change-password', 'HomeController@changePasswordStore')->name('change-password-store');
Route::get('/change-photo-profile', 'HomeController@changeImage')->name('change-photo-profile');
Route::post('/change-profile-image', 'HomeController@changeImageStore')->name('change-profile-image');

Route::group(['middleware' => ['auth.admin'],'prefix' => 'admin'], function () {
    Route::get('/', function () {
        return view('home');
    })->name('admin');

    // class/ kelas
    Route::get('/class', 'ClassRoomController@index')->name('class');
    Route::post('/class', 'ClassRoomController@store')->name('class-store');
    Route::patch('/class/{id}', 'ClassRoomController@update')->name('class-update');
    Route::delete('/class/{id}', 'ClassRoomController@destroy')->name('class-destroy');

    // teaher
    Route::get('/teachers', 'TeacherController@index')->name('teachers');
    Route::get('/teachers/new', 'TeacherController@create')->name('teachers-create');
    Route::post('/teachers', 'TeacherController@store')->name('teachers-store');
    Route::get('/teachers/update/{id}', 'TeacherController@edit')->name('teachers-edit');
    Route::patch('/teachers/{id}', 'TeacherController@update')->name('teachers-update');
    Route::delete('/teachers/{id}', 'TeacherController@destroy')->name('teachers-destroy');

    // student
    Route::get('/students', 'StudentController@index')->name('students');
    Route::get('/students/new', 'StudentController@create')->name('students-create');
    Route::post('/students', 'StudentController@store')->name('students-store');
    Route::get('/students/update/{id}', 'StudentController@edit')->name('students-edit');
    Route::patch('/students/{id}', 'StudentController@update')->name('students-update');
    Route::delete('/students/{id}', 'StudentController@destroy')->name('students-destroy');

    // exam type
    Route::get('/exam-type', 'ExamTypeController@index')->name('exam-type');
    Route::post('/exam-type', 'ExamTypeController@store')->name('exam-type-store');
    Route::patch('/exam-type/{id}', 'ExamTypeController@update')->name('exam-type-update');

    // score list
    Route::get('/score-list', 'HomeController@score')->name('admin-score-list');
});

Route::group(['middleware' => ['auth.teacher'],'prefix' => 'teacher'], function () {
    Route::get('/', function () {
        return view('home');
    })->name('teacher');

    // setting exam
    Route::get('/manage-exam', 'ExamController@index')->name('teacher-exam');
    Route::get('/manage-exam/create', 'ExamController@create')->name('teacher-exam-create');
    Route::post('/manage-exam', 'ExamController@store')->name('teacher-exam-store');
    Route::get('/manage-exam/{id}', 'ExamController@edit')->name('teacher-exam-edit');
    Route::patch('/manage-exam/{id}', 'ExamController@update')->name('teacher-exam-update');

    // exam question
    Route::get('/manage-exam/{id}/question', 'QuestionController@show')->name('teacher-question');
    Route::get('/manage-exam/{id}/question/create', 'QuestionController@create')->name('teacher-question-create');
    Route::post('/manage-exam/{id}/question', 'QuestionController@store')->name('teacher-question-store');
    Route::get('/manage-exam/{id}/question/{question_id}', 'QuestionController@edit')->name('teacher-question-edit');
    Route::patch('/manage-exam/{id}/question/{question_id}', 'QuestionController@update')->name('teacher-question-update');
    Route::delete('/manage-exam/{id}/question/{question_id}', 'QuestionController@destroy')->name('teacher-question-delete');
    Route::delete('/manage-exam/{id}/question-delete-all', 'QuestionController@destroyAll')->name('teacher-question-delete-all');

    // student list
    Route::get('/student-list', 'StudentController@index')->name('teacher-student-list');

    // score list
    Route::get('/score-list/{id}', 'HomeController@scoreDetail')->name('teacher-score-list');
});

Route::group(['middleware' => ['auth.student'],'prefix' => 'student'], function () {
    // exam list
    Route::get('/', 'HomeController@index')->name('student');

    // start exam
    Route::get('/exam-start/{id}', 'ExamController@start')->name('student-exam-start');

    // end exam
    Route::post('/exam-end/{id}', 'ExamController@end')->name('student-exam-end');

    // exam report
    Route::get('/score', 'HomeController@score')->name('student-score');
});
