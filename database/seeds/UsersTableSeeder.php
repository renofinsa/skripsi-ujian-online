<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // admin
        User::create([
            'name' => 'Administrator',
            'username' => 'admin',
            'password' => bcrypt('123123'),
            'gender' => 1,
            'religion' => 1,
            'birthplace' => 'Jakarta',
            'birthday' => date('Y-m-d'),
            'address' => 'Jl Kano Permai A11, Pluit, Jakarta Utara',
            'phone_number' => '082297770619',
            'photo_profile' => NULL,
            'role' => 1
        ]);

        // teacher
        User::create([
            'name' => 'Bambang Widjaja',
            'username' => '2221',
            'password' => bcrypt('123123'),
            'gender' => 1,
            'religion' => 1,
            'birthplace' => 'Tangerang',
            'birthday' => date('Y-m-d'),
            'address' => 'Jl Citra Indah, Tiga Raksa, Tangerang',
            'phone_number' => '082297770611',
            'photo_profile' => NULL,
            'role' => 2
        ]);

        User::create([
            'name' => 'Maria Agnes',
            'username' => '2222',
            'password' => bcrypt('123123'),
            'gender' => 2,
            'religion' => 2,
            'birthplace' => 'Jakarta',
            'birthday' => date('Y-m-d'),
            'address' => 'Jl Tobat, Kemanggisan, Jakarta Barat',
            'phone_number' => '082297770611',
            'photo_profile' => NULL,
            'role' => 2
        ]);
    }
}
