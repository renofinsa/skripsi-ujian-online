<?php

use Illuminate\Database\Seeder;
use App\ClassRoom;

class ClassTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ClassRoom::create([
            'semester' => 'Semester I (2020)',
            'name' => 'Kelas X',
            'homeroom' => 2
        ]);

        ClassRoom::create([
            'semester' => 'Semester II (2020)',
            'name' => 'Kelas X',
            'homeroom' => 2
        ]);
    }
}
