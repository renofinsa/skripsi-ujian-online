<?php

use Illuminate\Database\Seeder;
use App\ExamType;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ClassTableSeeder::class);
        $this->call(StudentTableSeeder::class);

        // exam type
        ExamType::create([
            'name' => 'Ujian Harian 1'
        ]);
        ExamType::create([
            'name' => 'Ujian Harian 2'
        ]);
        ExamType::create([
            'name' => 'Ujian Tengah Semester'
        ]);
        ExamType::create([
            'name' => 'Ujian Akhir Semester'
        ]);
    }
}
