<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Student;

class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // student
        User::create([
            'name' => 'Michele Patricia',
            'username' => '1111',
            'password' => bcrypt('123123'),
            'gender' => 2,
            'religion' => 2,
            'birthplace' => 'Jakarta',
            'birthday' => date('Y-m-d'),
            'address' => 'Jl Tobat, Kemanggisan, Jakarta Barat',
            'phone_number' => '082297570611',
            'photo_profile' => NULL,
            'role' => 3
        ]);

        Student::create([
            'user_id' => 4,
            'father_name' => 'Yodai Hazel',
            'mother_name' => 'Jakarta',
            'parent_phone_number' => '082279960122',
            'class_room_id' => 2,
        ]);

        User::create([
            'name' => 'Nicholas Saputra',
            'username' => '1112',
            'password' => bcrypt('123123'),
            'gender' => 1,
            'religion' => 1,
            'birthplace' => 'Jakarta',
            'birthday' => date('Y-m-d'),
            'address' => 'Jl Puri Kembangan, Jakarta Barat',
            'phone_number' => '082297770600',
            'photo_profile' => NULL,
            'role' => 3
        ]);

        Student::create([
            'user_id' => 5,
            'father_name' => 'Miler',
            'mother_name' => 'Aoi Sora',
            'parent_phone_number' => '082279990121',
            'class_room_id' => 2,
        ]);
    }
}
