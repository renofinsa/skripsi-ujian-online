<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <span class="brand-text font-weight-light">{{ Auth::user()->role }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                @if (Auth::user()->role == 'Admin')

                <li class="nav-header">ADMIN FEATURES</li>
                <li class="nav-item">
                    <a href="{{ route('admin') }}" class="nav-link {{ (request()->is('admin')) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-home"></i>
                        <p>Beranda</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('teachers') }}" class="nav-link {{ (request()->is('admin/teachers')) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-chalkboard-teacher"></i>
                        <p>Guru</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('students')}}" class="nav-link {{ (request()->is('admin/students')) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-users"></i>
                        <p>Siswa</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('class') }}" class="nav-link {{ (request()->is('admin/class')) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-school"></i>
                        <p>Kelas</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin-score-list') }}" class="nav-link {{ (request()->is('admin/score-list')) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-heart"></i>
                        <p>Nilai</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('exam-type') }}" class="nav-link {{ (request()->is('admin/exam-type')) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-file"></i>
                        <p>Jenis Ujian</p>
                    </a>
                </li>
                @endif
                @if (Auth::user()->role == "Guru")
                <li class="nav-header">TEACHER FEATURES</li>
                <li class="nav-item">
                    <a href="{{ route('teacher') }}" class="nav-link {{ (request()->is('teacher')) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-home"></i>
                        <p>Beranda</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('teacher-exam') }}" class="nav-link {{ (request()->is('teacher/manage-exam')) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-book"></i>
                        <p>Pengaturan Ujian</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('teacher-student-list') }}" class="nav-link {{ (request()->is('teacher/student-list')) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-users"></i>
                        <p>Data Siswa</p>
                    </a>
                </li>
                @endif
                @if (Auth::user()->role == 'Siswa')
                <li class="nav-header">STUDENT FEATURES</li>
                <li class="nav-item">
                    <a href="{{ route('student') }}" class="nav-link {{ (request()->is('student')) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-home"></i>
                        <p>Beranda</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('student-score') }}" class="nav-link {{ (request()->is('student/score')) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-book"></i>
                        <p>Nilai</p>
                    </a>
                </li>
                @endif
            </ul>
        </nav>
    </div>
</aside>
