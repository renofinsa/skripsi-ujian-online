<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- topbar navbar links -->
    <ul class="navbar-nav ml-auto">
    <!-- My Profile and Signout Dropdown -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-user"></i>
            {{ Auth::user()->name }}
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-item dropdown-header">Profil Pengguna</span>
            <div class="dropdown-divider"></div>
            <a href="{{ route('change-photo-profile') }}" class="dropdown-item">
                <i class="fas fa-user mr-2"></i>Ubah Foto Profil
            </a>
            <a href="{{ route('change-password') }}" class="dropdown-item">
                <i class="fas fa-cog mr-2"></i>Ubah Password
            </a>
            <a href="{{ route('logout') }}" class="dropdown-item">
                <i class="fas fa-lock mr-2"></i>Keluar Aplikasi
            </a>
            <div class="dropdown-divider"></div>
        </li>
    </ul>
</nav>
