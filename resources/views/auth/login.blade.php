@extends('layouts.public')

@section('content')
<div class="login-logo">
    <img width="200" src="{{ asset('img/logo.png') }}" alt="">
</div>
<!-- /.login-logo -->
<div class="card">
    <!-- /.login-card-body -->
    <div class="card-body login-card-body">
        <p class="login-box-msg">Masuk Aplikasi Sekolah</p>

        <form action="{{ route('login') }}" method="post">
            @csrf
            <div class="input-group mb-3">
                <input name="username" type="text" class="form-control @error('username') is-invalid @enderror" placeholder="NIS/NIP">
                <div class="input-group-append">
                    <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                    </div>
                </div>
                @error('username')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="input-group mb-3">
                <input name="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Kata Sandi">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="row">
            <div class="col-8">
                <div class="icheck-primary">
                <input name="remember" type="checkbox" id="remember">
                <label for="remember">
                    Tetap Masuk
                </label>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-4">
                <button type="submit" class="btn btn-primary btn-block">Masuk</button>
            </div>
            <!-- /.col -->
            </div>
        </form>

        <p class="mb-1 mt-4">
            Lupa Password Hubungi Guru Kelas
        </p>
    </div>
</div>

@endsection
