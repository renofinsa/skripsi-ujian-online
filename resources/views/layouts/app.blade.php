<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>@yield('title')</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <!-- OPTIONAL -->
    @yield('styles')
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('css/adminlte.min.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <style>
        .img-center {
            object-fit: cover;
            width: 200px;
            height: 200px;
        }
    </style>

</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
    <!-- Navbar -->
    @include('components.navbar')
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    @include('components.sidebar')
    <!-- Main Sidebar Container -->

    <!-- Wrapper Content -->
    <div class="content-wrapper">
        <!-- Alert Message -->
        @include('components.alert')
        <!-- Alert Message -->

        <!-- Content Breadcrumb -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">@yield('header')</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        @yield('breadcrumb')
                    </ol>
                </div>
                </div>
            </div>
        </div>
        <!-- /.Content Breadcrumb -->

        <!-- Main content -->
        @yield('content')
        <!-- Main content -->
    </div>
    <!-- Wrapper Content -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <strong>Copyright &copy; <span id="year"></span> <a href="#">Aplikasi Ujian Online</a>.</strong> All rights reserved.
        <div class="float-right d-none d-sm-inline-block"><b>Version</b> 1</div>
    </footer>
</div>
<!-- ./wrapper -->



<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- OPTIONAL -->
@yield('script')
<!-- AdminLTE App -->
<script src="{{ asset('js/adminlte.js') }}"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="{{ asset('js/demo.js') }}"></script>

<script>
    document.getElementById("year").innerHTML = new Date().getFullYear();
</script>
</body>
</html>
