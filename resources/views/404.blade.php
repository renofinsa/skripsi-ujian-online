@extends('layouts.public')

@section('title', 'Error 404')

@section('content')
<div class="card">
    <div class="card-body login-card-body">
        <h2 class="headline text-warning">Kode 404</h2>
        <h3><i class="fas fa-exclamation-triangle text-warning"></i> Waduh! halaman tidak ditemukan.</h3>
        <p>Apa kamu tersesat! Klik tautan berikut untuk kembali kehalaman sebelumnya.
        </p>
        <a class="btn btn-warning float-right text-white" href="{{ url()->previous() }}"><i class="fas fa-arrow-left"></i> Kembali</a>
    </div>
</div>

@endsection
