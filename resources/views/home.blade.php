@extends('layouts.app')

@section('title', 'Beranda')

@section('header')
    Beranda {{ Auth::user()->role }}
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item active">Beranda</li>
@endsection

@section('content')

@if (Auth::user()->role == "Siswa")
<section class="content">
    <div class="container-fluid">
        <h3>List Ujian</h3>
        <div class="row">
            @foreach ($data as $item)
            <div class="col-12 col-sm-6 col-md-3">
            <div class="card mb-3" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title"><b>{{ $item['exam']->title }}</b></h5>
                    <p class="card-text">
                        <ul>
                            <li>Tipe : {{ $item['exam']->type->name }}</li>
                            <li>Total Soal : {{ $item['exam']->question->count() }}</li>
                            <li>Durasi : {{ $item['exam']->duration }} Menit</li>
                            <li>Batas Waktu : {{ date('d-m-Y', strtotime($item['exam']->due_date)) }}</li>
                        </ul>
                        {{ $item['exam']->notes }}
                    </p>

                    @if (!empty($item['answer']))
                        <i class="text-success float-right fas fa-check-circle fa-2x"></i>
                    @endif
                    @if (empty($item['answer']))
                        {!! $item['exam']->question->count() < 10 ? '<i class="text-danger float-right fas fa-exclamation-circle fa-2x"></i>' : '<a href="'.route('student-exam-start', $item['exam']->id).'" class="btn btn-primary btn-sm  card-link float-right">Mulai</a>' !!}
                    @endif
                </div>
            </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endif

@endsection

@section('script')
<script src="{{ asset('js/pages/dashboard2.js') }}"></script>
@endsection
