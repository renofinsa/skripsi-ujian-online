@extends('layouts.app')

@section('title', 'Tambah Siswa')

@section('header')

@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('admin') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('students') }}">Siswa</a></li>
    <li class="breadcrumb-item active">Tambah</li>
@endsection

@section('content')
<section class="content">
    <div class="row justify-content-md-center">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Tambah Data Siswa Baru</h3>
                </div>
                <form action="{{ route('students-store') }}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">Nama Lengkap</label>
                            <input name="name" type="text" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="Nama Lengkap">
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="username">Nomer Induk Pengajar (NIP)</label>
                            <input name="username" type="text" class="form-control @error('username') is-invalid @enderror" value="{{ old('username') }}" placeholder="CTH: 112233">
                            <small class="text-danger">Digunakan untuk masuk aplikasi, Password default "123123"</small>
                            @error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="classroom">Kelas</label>
                            <select name="classroom" class="form-control @error('classroom') is-invalid @enderror">
                                <option value="">Pilih Kelas</option>
                                @foreach ($classroom as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }} - {{ $item->semester }}</option>
                                @endforeach
                            </select>
                            @error('classroom')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="row form-group">
                            <div class="col-6 form-group">
                                <label for="gender">Jenis Kelamin</label>
                                <select name="gender" class="form-control @error('gender') is-invalid @enderror" placeholder="Jenis Kelamin">
                                    <option value="">Pilih Jenis Kelamin</option>
                                    <option value="1">Laki-laki</option>
                                    <option value="2">Perempuan</option>
                                </select>
                                @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-6 form-group">
                                <label for="religion">Agama/ Kepercayaan</label>
                                <select name="religion" class="form-control @error('religion') is-invalid @enderror" placeholder="Jenis Kelamin">
                                    <option value="">Pilih Agama</option>
                                    <option value="Islam">Islam</option>
                                    <option value="Kristen">Kristen</option>
                                    <option value="Katolik">Katolik</option>
                                    <option value="Hindu">Hindu</option>
                                    <option value="Buddha">Buddha</option>
                                    <option value="Khonghucu">Khonghucu</option>
                                </select>
                                @error('religion')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-6 form-group">
                                <label for="birthplace">Tempat Lahir</label>
                                <input name="birthplace" type="text" class="form-control @error('birthplace') is-invalid @enderror" value="{{ old('birthplace') }}" placeholder="Tempat Lahir">
                                @error('birthplace')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-6 form-group">
                                <label for="birthday">Tanggal Lahir</label>
                                <input name="birthday" type="date" class="form-control @error('birthday') is-invalid @enderror" value="{{ old('birthday') }}" placeholder="Nama Lengkap">
                                @error('birthday')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone_number">Nomer Telepon</label>
                            <input name="phone_number" type="text" class="form-control @error('phone_number') is-invalid @enderror" value="{{ old('phone_number') }}" placeholder="Nomer Telepon, Cth: 6282297770XXX">
                            @error('phone_number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="address">Alamat Lengkap</label>
                            <textarea name="address" type="text" class="form-control @error('address') is-invalid @enderror" placeholder="Cth: Jl Merdeka Barat, No A11, Jakarta Pusat">{{ old('address') }}</textarea>
                            @error('address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <hr>
                        <div class="row form-group">
                            <div class="col-6 form-group">
                                <label for="father_name">Nama Ayah</label>
                                <input name="father_name" type="text" class="form-control @error('father_name') is-invalid @enderror" value="{{ old('father_name') }}" placeholder="Nama Ayah">
                                @error('father_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-6 form-group">
                                <label for="mother_name">Nama Ibu</label>
                                <input name="mother_name" type="text" class="form-control @error('mother_name') is-invalid @enderror" value="{{ old('mother_name') }}" placeholder="Nama Ibu">
                                @error('mother_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parent_phone_number">Nomer Telepon Orang Tua Wali</label>
                            <input name="parent_phone_number" type="text" class="form-control @error('parent_phone_number') is-invalid @enderror" value="{{ old('parent_phone_number') }}" placeholder="Cth: 6282297770XXX">
                            @error('parent_phone_number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('students') }}" class="btn btn-secondary float-right ml-3">Kembali</a>
                        <button type="submit" class="btn btn-primary float-right ml-3">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
