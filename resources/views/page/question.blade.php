@extends('layouts.app')

@section('title', 'List Soal')

@section('styles')
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('header')
List Soal (Kode Ujian {{$id}})
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('teacher') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('teacher-exam') }}">Pengaturan Ujian</a></li>
    <li class="breadcrumb-item active">List Soal</li>
@endsection

@section('content')
<section class="content">
    <div class="row">
      <div class="col-12">
        <!-- /.card -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title float-left">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#upload"><i class="fa fa-upload"></i> Upload Soal</button>
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteall"><i class="fa fa-trash"></i> Hapus Semua Soal</button>
            </h3>
            <h3 class="card-title float-right">
                <a href="{{ route('teacher-question-create', $id) }}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Soal</a>
            </h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                    <th>No</th>
                    <th>Soal</th>
                    <th>Jawaban A</th>
                    <th>Jawaban B</th>
                    <th>Jawaban C</th>
                    <th>Jawaban D</th>
                    <th>Jawaban E</th>
                    <th>Jawaban Benar</th>
                    <th width="50">Aksi</th>
                </tr>
              </thead>
              <tbody>
                    @php $no = 1 @endphp
                    @foreach ($data as $index => $item)
                        <tr>
                            <td> {{ $no++ }}</td>
                            <td> {{ $item->question }}</td>
                            <td> {{ $item->answer_a }}</td>
                            <td> {{ $item->answer_b }}</td>
                            <td> {{ $item->answer_c }}</td>
                            <td> {{ $item->answer_d }}</td>
                            <td> {{ $item->answer_e }}</td>
                            <td> {{ $item->answer_correct }}</td>
                            <td>
                                <a href="{{ route('teacher-question-edit', ['id'=>$id, 'question_id'=>$item->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil-alt"></i></a>
                                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete{{$item->id}}"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>

                        {{-- modal delete  --}}
                        <div class="modal fade" id="delete{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog modal-md" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel">Konfirmasi Hapus Soal</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    </div>
                                    <form action="{{ route('teacher-question-delete', ['id'=>$id, 'question_id'=>$item->id]) }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                        <div class="modal-body">
                                            Apa kamu yakin ingin menghapus soal ini?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Batalkan</button>
                                            <button type="submit" class="btn btn-primary">Hapus</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    {{-- modal upload  --}}
    <div class="modal fade" id="deleteall" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Upload Soal</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <form action="{{ route('teacher-question-delete-all', $id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="_method" value="DELETE">
                    <div class="modal-body">
                        Apa kamu yakin ingin menghapus semua soal?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batalkan</button>
                        <button type="submit" class="btn btn-primary">Hapus</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- modal hapus semua soal  --}}
    <div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Upload Soal</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <form action="{{ route('import-question') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Import Data Soal (format: csv,xls,xlsx)</label>
                            <input name="file" type="file" class="form-control" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                        </div>
                        <a class="mt-2" href="{{ asset('template/template-data-soal.xlsx') }}" target="_blank">Download Template</a>
                        <hr>
                        <p class="text-danger">Ketentuan: </p>
                        <ul>
                            <li>
                                Kode Ujian adalah <span class="text-danger">" {{ $id }} "</span>, Pastikan id/ kode ujian sesuai.
                            </li>
                            <li>
                                Jika terjadi pengurangan dan atau penambahan pada column sebaiknya column terakhir dihapus menggunakan metode delete column, agar data tidak terbaca NULL atau kosong.
                            </li>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Import</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</section>
@endsection

@section('script')
<!-- PAGE SCRIPTS -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
$(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endsection
