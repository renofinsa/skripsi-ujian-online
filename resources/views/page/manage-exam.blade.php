@extends('layouts.app')

@section('title', 'Pengaturan Ujian')

@section('styles')
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('header')
Pengaturan Ujian
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('teacher') }}">Beranda</a></li>
    <li class="breadcrumb-item active">Pengaturan Ujian</li>
@endsection

@section('content')
<section class="content">
    <div class="row">
      <div class="col-12">
        <!-- /.card -->

        <div class="card">
          <div class="card-header">
            <h3 class="card-title float-right">
                <a href="{{ route('teacher-exam-create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Buat Ujian</a>
            </h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                    <th>No</th>
                    <th>Mata Pelajaran</th>
                    <th>Jenis Ujian</th>
                    <th>Kelas</th>
                    <th>Durasi</th>
                    <th>Jumlah Soal</th>
                    <th>Status</th>
                    <th width="100">Aksi</th>
                </tr>
              </thead>
              <tbody>
                    @php $no = 1 @endphp
                    @foreach ($data as $index => $item)
                        <tr>
                            <td> {{ $no++ }}</td>
                            <td> {{ $item->title }}</td>
                            <td> {{ $item->type->name }}</td>
                            <td> {{ $item->classroom->name }} - {{ $item->classroom->semester }}</td>
                            <td> {{ $item->duration }} Menit</td>
                            <td> {{ $item->question->count() }}</td>
                            <td>
                                @if (date('d-m-Y', strtotime($item->due_date)) >= date('d-m-Y'))
                                    Aktif
                                @else
                                    Tidak Aktif
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('teacher-score-list', $item->id) }}" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> {{ $item->student_list->count() }}</a>
                                <a href="{{ route('teacher-exam-edit', $item->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil-alt"></i></a>
                                <a href="{{ route('teacher-question', $item->id) }}" class="btn btn-danger btn-sm"><i class="fa fa-file"></i></a>
                            </td>
                        </tr>
                    @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
@endsection

@section('script')
<!-- PAGE SCRIPTS -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
$(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endsection
