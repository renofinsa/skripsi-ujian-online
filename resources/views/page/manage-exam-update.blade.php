@extends('layouts.app')

@section('title', 'Ubah Ujian')

@section('header')
Ubah Ujian
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('admin') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('teacher-exam') }}">Pengaturan Ujian</a></li>
    <li class="breadcrumb-item active">Ubah</li>
@endsection

@section('content')
<section class="content">
    <div class="row justify-content-md-center">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Ubah Data Ujian</h3>
                </div>
                <form action="{{ route('teacher-exam-update', $data->id) }}" method="post">
                    @csrf
                    <input type="hidden" name="_method" value="PATCH">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="title">Judul/ Matepelajaran</label>
                            <input name="title" type="text" class="form-control @error('title') is-invalid @enderror" value="{{ $data->title }}" placeholder="Algoritma Dasar I">
                            @error('title')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exam_type_id">Tipe Ujian</label>
                            <select name="exam_type_id" class="form-control @error('exam_type_id') is-invalid @enderror">
                                <option value="{{ $data->exam_type_id }}">{{ $data->type->name }}</option>
                                @foreach ($examtype as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                            @error('exam_type_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="class_room_id">Kelas</label>
                            <select name="class_room_id" class="form-control @error('class_room_id') is-invalid @enderror">
                                <option value="{{ $data->class_room_id }}">{{ $data->classroom->name }} - {{ $data->classroom->semester }}</option>
                                @foreach ($classroom as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }} - {{ $item->semester }}</option>
                                @endforeach
                            </select>
                            @error('class_room_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="row form-group">
                            <div class="col-6 form-group">
                                <label for="duration">Durasi</label>
                                <input name="duration" type="number" maxlength="3" class="form-control @error('duration') is-invalid @enderror" value="{{ $data->duration }}" placeholder="Durasi Ujian">
                                <small class="text-danger">Diisi menggunakan satuan menit</small>
                                @error('duration')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-6 form-group">
                                <label for="due_date">Tanggal Pelaksanaan</label>
                                <input name="due_date" type="date" class="form-control @error('due_date') is-invalid @enderror" value="{{ date('Y-m-d', strtotime($data->due_date)) }}">
                                @error('due_date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="notes">Catatan</label>
                            <textarea name="notes" type="text" class="form-control @error('notes') is-invalid @enderror" placeholder="Jawab Pertanyaan dibawah ini">{{ $data->notes }}</textarea>
                            @error('notes')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('teacher-exam') }}" class="btn btn-secondary float-right ml-3">Kembali</a>
                        <button type="submit" class="btn btn-primary float-right ml-3">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
