@extends('layouts.app')

@section('title', 'Jenis Ujian')

@section('styles')
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('header')
Jenis Ujian
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('admin') }}">Beranda</a></li>
    <li class="breadcrumb-item active">Jenis Ujian</li>
@endsection

@section('content')
<section class="content">
    <div class="row">
      <div class="col-12">
        <!-- /.card -->

        <div class="card">
          <div class="card-header">
            <h3 class="card-title float-right">
                <!-- Large modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newSemester">Tambah Jenis Ujian</button>
            </h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                    <th>No</th>
                    <th>Jenis Ujian</th>
                    <th width="30">Aksi</th>
                </tr>
              </thead>
              <tbody>
                    @php $no = 1 @endphp
                    @foreach ($data as $index => $item)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td> {{ $item->name }}</td>
                            <td>
                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#updateSemester{{$item->id}}"><i class="fa fa-pencil-alt"></i></button>
                            </td>
                        </tr>
                        {{-- modal update semester --}}
                        <div class="modal fade" id="updateSemester{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog modal-sm" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel">Ubah Semester</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    </div>
                                    <form action="{{ route('exam-type-update', $item->id) }}" method="post">
                                        @csrf
                                        <input type="hidden" name="_method" value="PATCH">
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <input name="name" value="{{ $item->name }}" type="text" class="form-control" placeholder="Cth: Semester II (2020)">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary">Ubah Dan Simpan</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        {{-- modal update semester --}}
                    @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

{{-- modal new semester --}}
<div class="modal fade" id="newSemester" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Buat Semester Baru</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form action="{{ route('exam-type-store') }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <input name="name" type="text" class="form-control" placeholder="Cth: Semester II (2020)">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('script')
<!-- PAGE SCRIPTS -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
$(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endsection
