@extends('layouts.app')

@section('title', 'Ujian')

@section('header')
Ujian
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('student') }}">Beranda</a></li>
<li class="breadcrumb-item active">Memulai Ujian</li>
@endsection

@section('content')
<section class="content">
    <p class="btn btn-danger" style="position: fixed; color: #fff" id="timer"></p>
    <div class="row justify-content-md-center">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Soal</h3>
                    <input type="hidden" id="duration" value="30">
                </div>
                <div class="card-body">
                    <?php $no = 0; ?>
                    @foreach ($data->question()->inRandomOrder()->get() as $soal )
                    <?php $no++ ?>
                    <form class="" action="{{ route('student-exam-end', $id) }}" method="post">
                      @csrf
                      <input type="hidden" name="id[]" value="{{ $soal->id }}">
                      {{-- <input type="hidden" name="jumlah" value="{{ $soal->count()->where('id_quiz','=','')}}"> --}}
                      <input type="hidden" name="exam_id" value="{{ $id }}">
                      <input type="hidden" name="jawabanbenar[{{$soal->id}}]" value="{{ $soal->answer_correct }}">

                    <div class="card">
                      <div class="card-header">
                        <div class="row">
                          <div class="col-md-1">
                            <h3 class="card-title">{{ $no}} |</h3>
                        </div>
                          <div class="col-md-11">
                                <h3 class="card-title">{{ $soal->question}} </h3>
                          </div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="card-body table-responsive p-3">
                        <div class="form-group has-feedback">
                          <input type="radio" id="pilihan[{{$soal->id}}]a" name="pilihan[{{$soal->id}}]" value="a" style="margin-right: 10px"><label for="pilihan[{{$soal->id}}]a">A</label>
                          <input type="text" name="jawaban1" class="form-control" value="{{ $soal->answer_a}}" readonly>
                        </div>

                        <div class="form-group has-feedback">
                          <input type="radio" id="pilihan[{{$soal->id}}]b" name="pilihan[{{$soal->id}}]" value="b" style="margin-right: 10px"><label for="pilihan[{{$soal->id}}]b">B</label>
                          <input type="text" name="jawaban2" class="form-control" value="{{ $soal->answer_b}}" readonly>
                        </div>

                        <div class="form-group has-feedback">
                          <input type="radio" id="pilihan[{{$soal->id}}]c" name="pilihan[{{$soal->id}}]" value="c" style="margin-right: 10px"><label for="pilihan[{{$soal->id}}]c">C</label>
                          <input type="text" name="jawaban3" class="form-control" value="{{ $soal->answer_c}}" readonly>
                        </div>

                        <div class="form-group has-feedback">
                          <input type="radio" id="pilihan[{{$soal->id}}]d" name="pilihan[{{$soal->id}}]" value="d" style="margin-right: 10px"><label for="pilihan[{{$soal->id}}]d">D</label>
                          <input type="text" name="jawaban4" class="form-control" value="{{ $soal->answer_d}}" readonly>
                        </div>

                        <div class="form-group has-feedback">
                          <input type="radio" id="pilihan[{{$soal->id}}]e" name="pilihan[{{$soal->id}}]" value="e" style="margin-right: 10px"><label for="pilihan[{{$soal->id}}]e">E</label>
                          <input type="text" name="jawaban5" class="form-control" value="{{ $soal->answer_e}}" readonly>
                        </div>
                      </div>
                    </div>
                  @endforeach
                  <div class="form-group has-feedback">
                    <input class="btn btn-success form-control" type="submit" name="submit" value="Selesai" onclick="return confirm('Apakah Anda yakin dengan jawaban Anda?')">
                  </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
    <script>
        var day = new Date (),
        countDownDate = new Date ( day );
        countDownDate.setMinutes ( day.getMinutes() + parseFloat(document.getElementById("duration").value) );
        var x = setInterval(function() {
            var now = new Date().getTime();

            // Temukan jarak antara sekarang dan tanggal hitung mundur
            var distance = countDownDate - now;

            // Perhitungan waktu untuk hari, jam, menit dan detik
            // var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Keluarkan hasil dalam elemen dengan id = "demo"
            // document.getElementById("timer").innerHTML = days + "d " + hours + "h "
            // + minutes + "m " + seconds + "s ";
            document.getElementById("timer").innerHTML = hours + "h "
            + minutes + "m " + seconds + "s ";

            // Jika hitungan mundur selesai, tulis beberapa teks
            if (distance < 0) {
            clearInterval(x);
            document.getElementById("timer").innerHTML = "EXPIRED";
                alert("Waktu Habis!");
            }
        }, 1000);
    </script>
@endsection
