@extends('layouts.app')

@section('title', 'Ubah Password User')

@section('header')
    Ubah Password User
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/">Beranda</a></li>
    <li class="breadcrumb-item active">Ubah Password</li>
@endsection

@section('content')
<section class="content">
    <div class="row justify-content-md-center">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Ubah Password</h3>
                </div>
                <form action="{{ route('change-password-store') }}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="current">Password Lama</label>
                            <input name="current" type="text" class="form-control @error('current') is-invalid @enderror" value="{{ old('current') }}" placeholder="Password Lama">
                            @error('current')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="new">Password Baru</label>
                            <input name="new" type="text" class="form-control @error('new') is-invalid @enderror" value="{{ old('new') }}" placeholder="Password Baru">
                            @error('new')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="new_confirmation">Password Baru</label>
                            <input name="new_confirmation" type="text" class="form-control @error('new_confirmation') is-invalid @enderror" value="{{ old('new_confirmation') }}" placeholder="Ulangi Password Baru">
                            @error('new_confirmation')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary float-right ml-3">Simpan</button>
                        <a href="/" class="btn btn-secondary float-right ml-3">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
