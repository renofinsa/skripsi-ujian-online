@extends('layouts.app')

@section('title', 'Kelas')

@section('styles')
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('header')
    Kelas
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('admin') }}">Beranda</a></li>
    <li class="breadcrumb-item active">Kelas</li>
@endsection

@section('content')
<section class="content">
    <div class="row">
      <div class="col-12">
        <!-- /.card -->

        <div class="card">
          <div class="card-header">
            <h3 class="card-title float-right">
                <!-- Large modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#new">Buat Kelas Baru</button>
            </h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Kelas</th>
                    <th>Semester</th>
                    <th>Wali Kelas</th>
                    <th width="30">Aksi</th>
                </tr>
                </thead>
                <tbody>
                    @php $no = 1 @endphp
                    @foreach ($data as $index => $item)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td> {{ $item->name }}</td>
                            <td> {{ $item->semester }}</td>
                            <td> {{ $item->teacher->name }}</td>
                            <td class="row">
                                {{-- modal button update --}}
                                <button type="button" class="btn btn-primary btn-sm col-6" data-toggle="modal" data-target="#update{{$item->id}}"><i class="fa fa-pencil-alt"></i></button>

                                {{-- modal update class --}}
                                <div class="modal fade" id="update{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog modal-md" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel">Ubah Kelas</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <form action="{{ route('class-update', $item->id) }}" method="post">
                                                @csrf
                                                <input type="hidden" name="_method" value="PATCH">
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <input name="name" value="{{ $item->name }}" type="text" class="form-control" placeholder="Cth: Semester II (2020)" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <input name="semester" value="{{ $item->semester }}" type="text" class="form-control" placeholder="Cth: Semester I (2020)" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <select name="teacher" class="form-control" required>
                                                            <option value="{{ $item->homeroom }}">{{ $item->teacher->name }}</option>
                                                            @foreach ($teacher as $homeroom)
                                                                <option value="{{ $homeroom->id }}">{{ $homeroom->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                                    <button type="submit" class="btn btn-primary">Ubah Dan Simpan</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                {{-- modal update class --}}

                                {{-- modal button delete --}}
                                <button type="button" class="btn btn-danger btn-sm col-6" data-toggle="modal" data-target="#delete{{$item->id}}"><i class="fa fa-trash"></i></button>

                                {{-- modal delete  --}}
                                <div class="modal fade" id="delete{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog modal-md" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel">Hapus Data Kelas</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Jika anda menghapus data kelas maka semua data relasi yang berkaitan dengan kelas tersebut akan terhapus, apa anda setuju?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <form action="{{ route('class-destroy', $item->id)}}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="_method" value="delete">
                                                    <button class="btn btn-success"><i class="fa fa-download"></i> ya, Hapus Data</button>
                                                </form>

                                                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

{{-- modal new class --}}
<div class="modal fade" id="new" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Buat Kelas Baru</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form action="{{ route('class-store') }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <input name="name" type="text" class="form-control" placeholder="Cth: Kelas X" required>
                    </div>
                    <div class="form-group">
                        <input name="semester" type="text" class="form-control" placeholder="Cth: Semester I (2020)" required>
                    </div>
                    <div class="form-group">
                        <select name="teacher" class="form-control" required>
                            <option value="">Pilih Wali Kelas</option>
                            @foreach ($teacher as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('script')
<!-- PAGE SCRIPTS -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
$(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endsection
