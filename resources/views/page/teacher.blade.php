@extends('layouts.app')

@section('title', 'Guru')

@section('styles')
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('header')
    Guru
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('admin') }}">Beranda</a></li>
    <li class="breadcrumb-item active">Guru</li>
@endsection

@section('content')
<section class="content">
    <div class="row">
      <div class="col-12">
        <!-- /.card -->

        <div class="card">
          <div class="card-header">
            <h3 class="card-title float-left">
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#upload"><i class="fa fa-upload"></i> Upload Data Guru</button>
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#download"><i class="fa fa-download"></i> Download Data Guru</button>
            </h3>
            <h3 class="card-title float-right">
                <a href="{{ route('teachers-create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Guru</a>
            </h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                    <th>No</th>
                    <th width="150">Foto</th>
                    <th>NIP</th>
                    <th>Nama</th>
                    <th>Jenis Kelamin</th>
                    <th>Nomer Telepon</th>
                    <th width="50">Aksi</th>
                </tr>
              </thead>
              <tbody>
                    @php $no = 1 @endphp
                    @foreach ($data as $index => $item)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td><img class="img-center" src="{{ !$item->photo_profile ? asset('img/default.png') : asset('storage/img/profile/'.$item->photo_profile.'') }}" alt=""></td>
                            <td> {{ $item->username }}</td>
                            <td> {{ $item->name }}</td>
                            <td> {{ $item->gender }}</td>
                            <td> {{ $item->phone_number }}</td>
                            <td>
                                <a href="{{ route('teachers-edit', $item->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil-alt"></i></a>

                                {{-- modal button delete --}}
                                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete{{$item->id}}"><i class="fa fa-trash"></i></button>

                                {{-- modal delete  --}}
                                <div class="modal fade" id="delete{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog modal-md" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel">Hapus Data Guru</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Jika anda menghapus data guru maka semua data relasi yang berkaitan dengan guru tersebut akan terhapus, apa anda setuju?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <form action="{{ route('teachers-destroy', $item->id)}}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="_method" value="delete">
                                                    <button class="btn btn-success"><i class="fa fa-download"></i> ya, Hapus Data</button>
                                                </form>

                                                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    {{-- modal upload  --}}
    <div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Upload Data Guru</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <form action="{{ route('import-teacher') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Import Data Guru (format: csv,xls,xlsx)</label>
                            <input name="file" type="file" class="form-control" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                        </div>
                        <a class="mt-2" href="{{ asset('template/template-data-guru.xlsx') }}" target="_blank">Download Template</a>
                        <hr>
                        <p class="text-danger">Ketentuan:</p>
                        <ul>
                            <li>
                                Pastikan NIP tidak duplikat dan semua column terisi sesuai ketentuan.
                            </li>
                            <li>
                                Jika terjadi pengurangan dan atau penambahan pada column sebaiknya column terakhir dihapus menggunakan metode delete column, agar data tidak terbaca NULL atau kosong.
                            </li>
                            <li>
                                default password "123123".
                            </li>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Import</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- modal download  --}}
    <div class="modal fade" id="download" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Download Data Guru</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <p>Apa anda yakin ingin download semua data guru?</p>
                </div>
                <div class="modal-footer">
                    <a href="{{ route('export-teacher')}}" target="_blank" class="btn btn-success"><i class="fa fa-download"></i> Iya, Download Sekarang</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

</section>
@endsection

@section('script')
<!-- PAGE SCRIPTS -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
$(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endsection
