@extends('layouts.app')

@section('title', 'Ubah Soal')

@section('header')
Ubah Soal
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('teacher') }}">Beranda</a></li>
<li class="breadcrumb-item"><a href="{{ route('teacher-exam') }}">Pengaturan Ujian</a></li>
<li class="breadcrumb-item"><a href="{{ route('teacher-question', $id) }}">List Soal</a></li>
<li class="breadcrumb-item active">Ubah Soal</li>
@endsection

@section('content')
<section class="content">
    <div class="row justify-content-md-center">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Ubah Soal</h3>
                </div>
                <form action="{{ route('teacher-question-update', ['id'=>$id, 'question_id'=>$data->id]) }}" method="post">
                    @csrf
                    <input type="hidden" name="_method" value="PATCH">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="question">Soal</label>
                            <input name="question" type="text" class="form-control @error('question') is-invalid @enderror" value="{{ $data->question }}" placeholder="Soal, Cth: Ibu Kota Indonesia Adalah ...">
                            @error('question')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <hr>
                        <h5>Jawaban</h5>
                        <hr>
                        <div class="form-group">
                            <label for="answer_a">A</label>
                            <input name="answer_a" type="text" class="form-control @error('answer_a') is-invalid @enderror" value="{{ $data->answer_a }}" placeholder="Cth: Bali">
                            @error('answer_a')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="answer_b">B</label>
                            <input name="answer_b" type="text" class="form-control @error('answer_b') is-invalid @enderror" value="{{ $data->answer_b }}" placeholder="Cth: DKI Jakarta">
                            @error('answer_b')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="answer_c">C</label>
                            <input name="answer_c" type="text" class="form-control @error('answer_c') is-invalid @enderror" value="{{ $data->answer_c }}" placeholder="Cth: Yogyakarta">
                            @error('answer_c')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="answer_d">D</label>
                            <input name="answer_d" type="text" class="form-control @error('answer_d') is-invalid @enderror" value="{{ $data->answer_d }}" placeholder="Cth: Bandung">
                            @error('answer_d')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="answer_e">E</label>
                            <input name="answer_e" type="text" class="form-control @error('answer_e') is-invalid @enderror" value="{{ $data->answer_e }}" placeholder="Cth: Kalimantan">
                            @error('answer_e')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="answer_correct">Jawaban Benar</label>
                            <div class="row pl-3">
                                <div class="col-2 custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" value="a" id="a" name="answer_correct"
                                    @if ($data->answer_correct == 'a')
                                        checked
                                    @endif >
                                    <label for="a" class="custom-control-label">Jawaban A</label>
                                </div>
                                <div class="col-2 custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" value="b" id="b" name="answer_correct"
                                    @if ($data->answer_correct == 'b')
                                        checked
                                    @endif >
                                    <label for="b" class="custom-control-label">Jawaban B</label>
                                </div>
                                <div class="col-2 custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" value="c" id="c" name="answer_correct"
                                    @if ($data->answer_correct == 'c')
                                        checked
                                    @endif >
                                    <label for="c" class="custom-control-label">Jawaban C</label>
                                </div>
                                <div class="col-2 custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" value="d" id="d" name="answer_correct"
                                    @if ($data->answer_correct == 'd')
                                        checked
                                    @endif >
                                    <label for="d" class="custom-control-label">Jawaban D</label>
                                </div>
                                <div class="col-2 custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" value="e" id="e" name="answer_correct"
                                    @if ($data->answer_correct == 'e')
                                        checked
                                    @endif >
                                    <label for="e" class="custom-control-label">Jawaban E</label>
                                </div>
                            </div>

                            @error('answer_correct')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('teacher-question', $id) }}" class="btn btn-secondary float-right ml-3">Kembali</a>
                        <button type="submit" class="btn btn-primary float-right ml-3">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
