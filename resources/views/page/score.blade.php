@extends('layouts.app')

@section('title', 'Daftar Nilai')

@section('styles')
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('header')
Daftar Nilai
@endsection

@section('breadcrumb')
@if (Auth::user()->role == 'Guru')
    <li class="breadcrumb-item"><a href="{{ route('teacher') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('teacher-exam') }}">Pengaturan Ujian</a></li>
    <li class="breadcrumb-item active">Daftar Nilai</li>
@else
    <li class="breadcrumb-item"><a href="{{ route('admin') }}">Beranda</a></li>
    <li class="breadcrumb-item active">Daftar Nilai</li>
@endif
@endsection

@section('content')
<section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
            <div class="card-header">
                @if (!empty($id))
                    <a class="btn btn-success" target="_blank" href="{{ route('export-score-detail', $id) }}"><i class="fa fa-download"></i> Download</a>
                @else
                    <a class="btn btn-success" target="_blank" href="{{ route('export-score') }}"><i class="fa fa-download"></i> Download</a>
                @endif
            </div>
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                    <th>No</th>
                    @if (Auth::user()->role != 'Siswa')
                        <th>Nama Siswa</th>
                    @endif
                    <th>Mata Pelajaran</th>
                    <th>Jenis Ujian</th>
                    <th>Nilai</th>
                    <th>Tanggal Ujian</th>
                </tr>
              </thead>
              <tbody>
                    @php $no = 1 @endphp
                    @foreach ($data as $index => $item)
                        <tr>
                            <td> {{ $no++ }}</td>
                            @if (Auth::user()->role != 'Siswa')
                                <td> {{ $item->student->name }}</td>
                            @endif
                            <td> {{ $item->exam->title }}</td>
                            <td> {{ $item->exam->type->name }}</td>
                            <td> {{ $item->score }}</td>
                            <td> {{ date('d-m-Y', strtotime($item->created_at)) }}</td>
                        </tr>
                    @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
@endsection

@section('script')
<!-- PAGE SCRIPTS -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
$(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endsection
