@extends('layouts.app')

@section('title', 'Ubah Foto Profil')

@section('header')
Ubah Foto Profil
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/">Beranda</a></li>
    <li class="breadcrumb-item active">Ubah Foto Profil</li>
@endsection

@section('content')
<section class="content">
    <div class="row justify-content-md-center">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Ubah Foto Profil</h3>
                </div>
                <form action="{{ route('change-profile-image') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <label for="current">Foto Saat ini</label>
                        <div class="form-group">
                            <img class="img-center" src="{{ !$user->photo_profile ? asset('img/default.png') : asset('storage/img/profile/'.$user->photo_profile.'') }}" alt="">
                        </div>
                        <div class="form-group">
                            <label for="cover">Upload Foto Baru</label>
                            <input name="old_cover" type="hidden" class="form-control @error('old_cover') is-invalid @enderror"
                            value="{{ !$user->photo_profile ? NULL : $user->photo_profile }}">
                            <input name="cover" type="file" class="form-control @error('cover') is-invalid @enderror" value="{{ old('cover') }}" required accept="image/*">
                            @error('cover')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary float-right ml-3">Simpan</button>
                        <a href="/" class="btn btn-secondary float-right ml-3">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
