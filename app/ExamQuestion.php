<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamQuestion extends Model
{
    protected $fillable = [
        'id',
        'exam_id',
        'question',
        'answer_a',
        'answer_b',
        'answer_c',
        'answer_d',
        'answer_e',
        'answer_correct'
    ];
}
