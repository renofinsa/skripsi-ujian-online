<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamAnswer extends Model
{
    protected $fillable = [
        'id',
        'exam_id',
        'student_id',
        'score',
        'correct',
        'failure',
        'empty',
        'status'
    ];

    public function student(){
        return $this->belongsTo(User::class, 'student_id');
    }

    public function exam(){
        return $this->belongsTo(Exam::class, 'exam_id');
    }
}
