<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AuthTeacher
{
   // this is middleware for handle routes
    public function handle($request, Closure $next, $guard = null)
    {
        try {
            if (Auth::guard($guard)->user()->role == 'Guru') {
                return $next($request);
            } else {
                return redirect('/404');
            }
        } catch (\Throwable $th) {
            return redirect('/logout');
        }
    }
}
