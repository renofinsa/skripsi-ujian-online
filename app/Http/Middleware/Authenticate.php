<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Closure;
use Auth;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        // if (! $request->expectsJson()) {
        //     return route('login');
        // }
        try {
            if (Auth::guard($guard)->user()->role == 'Admin') {
                return $next($request);
            } else if (Auth::guard($guard)->user()->role == 'Guru') {
                return $next($request);
            } else if (Auth::guard($guard)->user()->role == 'Siswa') {
                return $next($request);
            } else {
                return redirect('/logout');
            }
        } catch (\Throwable $th) {
            return redirect('/logout');
        }
    }
}
