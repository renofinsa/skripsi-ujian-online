<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExamQuestion;

class QuestionController extends Controller
{
    public function create($id)
    {
        return view('page.question-create', compact('id'));
    }

    public function store(Request $request, $id)
    {
        // validate payload
        $get = $request->validate([
            'question' => ['required'],
            'answer_a' => ['required'],
            'answer_b' => ['required'],
            'answer_c' => ['required'],
            'answer_d' => ['required'],
            'answer_e' => ['required'],
            'answer_correct' => ['required']
        ]);

        // insert
        $data = new ExamQuestion();
        $data->id = NULL;
        $data->exam_id = $id;
        $data->question = $get['question'];
        $data->answer_a = $get['answer_a'];
        $data->answer_b = $get['answer_b'];
        $data->answer_c = $get['answer_c'];
        $data->answer_d = $get['answer_d'];
        $data->answer_e = $get['answer_e'];
        $data->answer_correct = $get['answer_correct'];
        $data->save();

        return redirect(route('teacher-question', $id))->with('alert','Soal berhasil ditambah.');
    }

    public function show($id)
    {
        $data = ExamQuestion::where('exam_id', $id)->get();
        return view('page.question', compact('data', 'id'));
    }

    public function edit($id, $question_id)
    {
        $data = ExamQuestion::find($question_id);
        return view('page.question-update', compact('data', 'id'));
    }

    public function update(Request $request, $id, $question_id)
    {
        // validate payload
        $get = $request->validate([
            'question' => ['required'],
            'answer_a' => ['required'],
            'answer_b' => ['required'],
            'answer_c' => ['required'],
            'answer_d' => ['required'],
            'answer_e' => ['required'],
            'answer_correct' => ['required']
        ]);

        // insert
        $data = ExamQuestion::find($question_id);
        $data->question = $get['question'];
        $data->answer_a = $get['answer_a'];
        $data->answer_b = $get['answer_b'];
        $data->answer_c = $get['answer_c'];
        $data->answer_d = $get['answer_d'];
        $data->answer_e = $get['answer_e'];
        $data->answer_correct = $get['answer_correct'];
        $data->save();

        return redirect(route('teacher-question', $id))->with('alert','Soal berhasil diubah.');
    }

    // single delete
    public function destroy($id, $question_id)
    {
        $data = ExamQuestion::find($question_id);
        $data->delete();
        return redirect(route('teacher-question', $id))->with('alert','Soal berhasil dihapus.');
    }

    // delete all
    public function destroyAll($id)
    {
        $data = ExamQuestion::where('exam_id', $id)->get();
        foreach ($data as $item) {
            $delete = ExamQuestion::find($item->id);
            $delete->delete();
        }
        return redirect(route('teacher-question', $id))->with('alert','Semua soal berhasil dihapus.');
    }
}
