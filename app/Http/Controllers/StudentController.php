<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Student;
use App\ClassRoom;

class StudentController extends Controller
{
    public function index()
    {
        $data = User::where('role', 3)->orderBy('name', 'asc')->get();
        return view('page.student', compact('data'));
    }

    public function create()
    {
        $classroom = ClassRoom::all();
        return view('page.student-create', compact('classroom'));
    }

    public function store(Request $request)
    {
        // validate payload
        $get = $request->validate([
            'name' => ['required', 'string', 'max:50', 'min:3'],
            'username' => ['required', 'string', 'min:4', 'unique:users'],
            'gender' => ['required'],
            'religion' => ['required'],
            'birthplace' => ['required'],
            'birthday' => ['required'],
            'phone_number' => ['required', 'min:10', 'max:14'],
            'address' => ['required'],
            'classroom' => ['required'],
            'father_name' => ['required', 'max:50', 'min:3'],
            'mother_name' => ['required', 'max:50', 'min:3'],
            'parent_phone_number' => ['required', 'min:10', 'max:14']
        ]);

        // insert
        $data = new User;
        $data->name = $get['name'];
        $data->username = $get['username'];
        $data->password = bcrypt('123123');
        $data->gender = $get['gender'];
        $data->religion = $get['religion'];
        $data->birthplace = $get['birthplace'];
        $data->birthday = $get['birthday'];
        $data->address = $get['address'];
        $data->phone_number = $get['phone_number'];
        $data->photo_profile = NULL;
        $data->role = 3;
        $data->save();

        // insert detail student
        $student = new Student();
        $student->user_id = $data->id;
        $student->class_room_id = $get['classroom'];
        $student->father_name = $get['father_name'];
        $student->mother_name = $get['mother_name'];
        $student->parent_phone_number = $get['parent_phone_number'];
        $student->save();

        // response success
        return redirect(route('students'))->with('alert','Siswa berhasil ditambah.');
    }

    public function edit($id)
    {
        $classroom = ClassRoom::all();
        $data = User::find($id);
        return view('page.student-update', compact('classroom', 'data'));
    }

    public function update(Request $request, $id)
    {
        // try {
            // validate payload
            $get = $request->validate([
                'name' => ['required', 'string', 'max:50', 'min:3'],
                'gender' => ['required'],
                'religion' => ['required'],
                'birthplace' => ['required'],
                'birthday' => ['required'],
                'phone_number' => ['required', 'min:10', 'max:14'],
                'address' => ['required'],
                'classroom' => ['required'],
                'father_name' => ['required', 'max:50', 'min:3'],
                'mother_name' => ['required', 'max:50', 'min:3'],
                'parent_phone_number' => ['required', 'min:10', 'max:14']
            ]);

            // insert
            $data = User::find($id);
            $data->name = $get['name'];
            $data->password = bcrypt('123123');
            $data->gender = $get['gender'];
            $data->religion = $get['religion'];
            $data->birthplace = $get['birthplace'];
            $data->birthday = $get['birthday'];
            $data->address = $get['address'];
            $data->phone_number = $get['phone_number'];
            $data->save();

            // insert detail student
            $student = Student::where('user_id', $data->id)->first();
            $student->class_room_id = $get['classroom'];
            $student->father_name = $get['father_name'];
            $student->mother_name = $get['mother_name'];
            $student->parent_phone_number = $get['parent_phone_number'];
            $student->save();

            // response success
            return redirect(route('students'))->with('alert','Siswa berhasil diubah.');
        // } catch (\Throwable $th) {
        //     // response failure
        //     return redirect(route('students'))->with('alert-warning','Sistem bermasalah.');
        // }
    }

    public function destroy($id)
    {
        // get student
        $student = Student::where('user_id', $id)->delete();

        // fetch user as student and delete
        $user = User::find($id);
        $user->delete();

        // response
        return redirect()->back()->with('alert','Data siswa berhasil dihapus.');
    }
}
