<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// import
use App\Imports\StudentImport;
use App\Imports\TeacherImport;
use App\Imports\QuestionImport;
// export
use App\Export\StudentExport;
use App\Exports\TeacherExport;
use App\Exports\scoreExport;
use App\Exports\scoreAllExport;
use Maatwebsite\Excel\Facades\Excel;
use Auth;


class ExcelController extends Controller
{
    public function uploadDataStudent(Request $request)
    {
        // try {
            // validasi
            $this->validate($request, [
                'file' => 'required|mimes:csv,xls,xlsx'
            ]);

            // catch file excel
            $file = $request->file('file');

            // make a unique file name
            $new_file_name = rand().$file->getClientOriginalName();

            // upload to folder file_upload_data_siswa on publicf older
            $file->move('file_upload_data_siswa',$new_file_name);

            // import data
            Excel::import(new StudentImport, public_path('/file_upload_data_siswa/'.$new_file_name));

            // return success
            return redirect()->back()->with('alert', 'Import berhasil!');
        // } catch (\Throwable $th) {
        //     // return failure
        //     return redirect()->back()->with('alert-warning', 'Terjadi kesalahan pada sistem, atau data duplikat!');
        // }
    }

    public function uploadDataTeacher(Request $request)
    {
        try {
            // validasi
            $this->validate($request, [
                'file' => 'required|mimes:csv,xls,xlsx'
            ]);

            // catch file excel
            $file = $request->file('file');

            // make a unique file name
            $new_file_name = rand().$file->getClientOriginalName();

            // upload to folder file_upload_data_siswa on publicf older
            $file->move('file_upload_data_guru',$new_file_name);

            // import data
            Excel::import(new TeacherImport, public_path('/file_upload_data_guru/'.$new_file_name));

            // return success
            return redirect()->back()->with('alert', 'Import berhasil!');
        } catch (\Throwable $th) {
            // return failure
            return redirect()->back()->with('alert-warning', 'Terjadi kesalahan pada sistem, atau data duplikat!');
        }
    }

    public function uploadDataQuestion(Request $request)
    {
        // try {
            // validasi
            $this->validate($request, [
                'file' => 'required|mimes:csv,xls,xlsx'
            ]);

            // catch file excel
            $file = $request->file('file');

            // make a unique file name
            $new_file_name = rand().$file->getClientOriginalName();

            // upload to folder file_upload_data_siswa on publicf older
            $file->move('file_upload_data_soal',$new_file_name);

            // import data
            Excel::import(new QuestionImport, public_path('/file_upload_data_soal/'.$new_file_name));

            // return success
            return redirect()->back()->with('alert', 'Import berhasil!');
        // } catch (\Throwable $th) {
        //     // return failure
        //     return redirect()->back()->with('alert-warning', 'Terjadi kesalahan pada sistem, atau data duplikat!');
        // }
    }

    public function downloadDataStudent(Request $request)
    {
        return Excel::download(new StudentExport, 'download-data-siswa.xlsx');
    }

    public function downloadDataTeacher(Request $request)
    {
        return Excel::download(new TeacherExport, 'download-data-guru.xlsx');
    }

    public function downloadDataScore(Request $request)
    {
        return Excel::download(new scoreAllExport, 'download-data-nilai.xlsx');
    }

    public function downloadDataScoreDetail(Request $request, $id)
    {
        return Excel::download(new scoreExport($id), 'download-data-nilai.xlsx');
    }
}
