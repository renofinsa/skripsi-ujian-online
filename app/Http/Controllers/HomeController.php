<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Exam;
use App\User;
use App\ExamAnswer;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        try {
            if (Auth::user()->role == 'Admin'){
                return redirect(route('admin'));
            } else if (Auth::user()->role == 'Guru') {
                return redirect(route('teacher'));
            } else if (Auth::user()->role == 'Siswa') {
                $user = Auth::user()->student;
                $data = [];
                $exam = Exam::where('class_room_id', $user->class_room_id)->orderBy('id', 'desc')->get();
                foreach ($exam as $key => $item) {
                    $answer = ExamAnswer::where([['student_id', Auth::user()->id],['exam_id', $item->id]])->first();
                    $payload['exam'] = $item;
                    $payload['answer'] = $answer;
                    array_push($data, $payload);
                }

                return view('home', compact('data'));
            }else{
                return route('logout');
            }
        } catch (\Throwable $th) {
            return route('logout');
        }
    }

    public function score()
    {
        if (Auth::user()->role == 'Admin') {
            $data = ExamAnswer::orderBy('id', 'desc')->get();
        } else {
            $data = ExamAnswer::where('student_id', Auth::user()->id)->get();
        }
        return view('page.score', compact('data'));
    }

    public function scoreDetail($id)
    {
        $data = ExamAnswer::where('exam_id', $id)->get();
        return view('page.score', compact('data', 'id'));
    }

    public function changePassword()
    {
        return view('page.change-password');
    }

    public function changePasswordStore(Request $request)
    {
        // get current user
        $user = Auth::user();

        // validate payload
        $get = $request->validate([
            'current' => ['required', 'min:6'],
            'new' => ['required', 'min:6', 'confirmed']
        ]);

        if (Hash::check($get['current'], $user->password)) {
            // The passwords match...
            $data = User::find($user->id);
            $data->password = bcrypt($get['new']);
            $data->save();

            // return true
            return redirect()->back()->with('alert','Password berhasil diubah.');
        }

        // return false
        return redirect()->back()->with('alert-warning','Password tidak sesuai, coba lagi.');
    }

    public function changeImage()
    {
        $user = Auth::user();
        // dd($user);
        return view('page.change-photo-profile', compact('user'));
    }

    public function changeImageStore(Request $request)
    {
        // get input post
        $old_cover = $request->get('old_cover');

        // handle upload image
        if($request->hasFile('cover')){
            $filenameWithExt = $request->file('cover')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('cover')->getClientOriginalExtension();
            $file_store_name = $filename.'_'.time().'.'.$extension;
            $path = $request->file('cover')->storeAs('public/img/profile/', $file_store_name);
            $current_path = 'storage/img/profile/'.$old_cover;

            // if last profile exist
            if ($old_cover) {
                if(file_exists($current_path)) {
                    unlink($current_path);
                }
            }
        }else{
            $file_store_name = $old_cover;
        }

        // get current user
        $user = Auth::user();

        // update process
        $data = User::find($user->id);
        $data->photo_profile = $file_store_name;
        $data->save();

        // response
        return redirect()->back()->with('alert-success','Foto berhasil diubah.');
    }
}
