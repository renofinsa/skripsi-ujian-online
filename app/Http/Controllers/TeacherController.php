<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\ClassRoom; // user
use App\Exam; // classroom
use App\ExamQuestion; // exam
use App\Student; // classroom

class TeacherController extends Controller
{
    public function index()
    {
        $data = User::where('role', 2)->orderBy('name', 'asc')->get();
        return view('page.teacher', compact('data'));
    }

    public function create()
    {
        return view('page.teacher-create');
    }

    public function store(Request $request)
    {
        try {
            // validate payload
            $get = $request->validate([
                'name' => ['required', 'string', 'max:50', 'min:3'],
                'username' => ['required', 'string', 'min:4', 'unique:users'],
                'gender' => ['required'],
                'religion' => ['required'],
                'birthplace' => ['required'],
                'birthday' => ['required'],
                'phone_number' => ['required', 'min:10', 'max:14'],
                'address' => ['required']
            ]);

            // insert
            $data = new User;
            $data->name = $get['name'];
            $data->username = $get['username'];
            $data->password = bcrypt('123123');
            $data->gender = $get['gender'];
            $data->religion = $get['religion'];
            $data->birthplace = $get['birthplace'];
            $data->birthday = $get['birthday'];
            $data->address = $get['address'];
            $data->phone_number = $get['phone_number'];
            $data->photo_profile = NULL;
            $data->role = 2;
            $data->save();

            // response success
            return redirect(route('teachers'))->with('alert','Data guru berhasil ditambah.');
        } catch (\Throwable $th) {
            // response failure
            return redirect(route('teachers'))->with('alert-warning','Sistem bermasalah.');
        }
    }

    public function edit($id)
    {
        $data = User::find($id);
        return view('page.teacher-update', compact('data'));
    }

    public function update(Request $request, $id)
    {
        try {
            // validate payload
            $get = $request->validate([
                'name' => ['required', 'string', 'max:50', 'min:3'],
                'gender' => ['required'],
                'religion' => ['required'],
                'birthplace' => ['required'],
                'birthday' => ['required'],
                'phone_number' => ['required', 'min:10', 'max:14'],
                'address' => ['required']
            ]);

            // insert
            $data = User::find($id);
            $data->name = $get['name'];
            $data->gender = $get['gender'];
            $data->religion = $get['religion'];
            $data->birthplace = $get['birthplace'];
            $data->birthday = $get['birthday'];
            $data->address = $get['address'];
            $data->phone_number = $get['phone_number'];
            $data->save();

            // response success
            return redirect(route('teachers'))->with('alert','Data guru berhasil diubah.');
        } catch (\Throwable $th) {
            // response failure
            return redirect(route('teachers'))->with('alert-warning','Sistem bermasalah.');
        }
    }

    public function destroy($id)
    {
        // get user as teacher
        $user = User::find($id);

        // get classroom with user teacher id
        $classroom = ClassRoom::where('homeroom', $user->id)->get();

        // fetch and delete classroom
        if ($classroom) {
            for ($i=0; $i < $classroom->count(); $i++) {
                $classroom_delete = ClassRoom::find($classroom[$i]->id);

                // get exam
                $exam = Exam::where('class_room_id', $classroom[$i]->id)->get();

                // fetch and delete exam
                if ($exam) {
                    for ($i=0; $i < $exam->count(); $i++) {
                        $exam_delete = Exam::find($exam[$i]->id);

                        // get exam question
                        $examQuestion = ExamQuestion::where('exam_id', $exam[$i]->id)->get();

                        if ($examQuestion) {
                            for ($i=0; $i < $examQuestion->count(); $i++) {
                                $examQuestion_delete = ExamQuestion::find($examQuestion[$i]->id);
                                $examQuestion_delete->delete(); // exam question deleted
                            }
                        }

                        // exam deleted
                        $exam_delete->delete();
                    }
                }

                // get student
                $student = Student::where('class_room_id', $classroom[$i]->id)->get();
                if ($student) {
                    for ($i=0; $i < $student->count(); $i++) {
                        $student_delete = Student::find($student[$i]->id);

                        // fetch user as student and delete
                        $user_delete = User::find($student[$i]->user_id);
                        $user_delete->delete();

                        // student deleted
                        $student_delete->delete();
                    }
                }

                // classroom deleted
                $classroom_delete->delete();
            }
        }

        // deleted teacher
        $user->delete();

        // response
        return redirect()->back()->with('alert','Data guru berhasil dihapus.');
    }
}
