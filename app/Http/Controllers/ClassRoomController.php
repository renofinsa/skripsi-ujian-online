<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClassRoom;
use App\User;
use App\Exam;
use App\ExamQuestion;
use App\Student;

class ClassRoomController extends Controller
{
    // page class/ kelas
    public function index()
    {
        $data = ClassRoom::orderBy('id', 'desc')->get();
        $teacher = User::where('role', 2)->orderBy('name', 'asc')->get();
        return view('page.classroom', compact('data', 'teacher'));
    }

    public function store(Request $request)
    {
        try {
            // check available name
            $check = ClassRoom::where([['name', $request->name],['semester', $request->semester]])->first();
            if ($check) {
                return redirect()->back()->with('alert-warning', 'Data telah tersedia, coba dengan data lain.');
            }
            // insert data
            $data = new ClassRoom();
            $data->name = $request->name;
            $data->semester = $request->semester;
            $data->homeroom = $request->teacher;
            $data->save();

            // response success
            return redirect()->back()->with('alert', 'Kelas baru berhasil dibuat.');
        } catch (\Throwable $th) {
            // response failure
            return redirect()->back()->with('alert-warning', 'Terjadi kesalahan pada sistem!.');
        }
    }

    public function update(Request $request, $id)
    {
        try {
            // check available name
            $check = ClassRoom::where([['name', $request->name],['semester', $request->semester]])->first();
            if ($check) {
                return redirect()->back()->with('alert-warning', 'Data telah tersedia, coba dengan data lain.');
            }

            // insert data
            $data = ClassRoom::find($id);
            $data->name = $request->name;
            $data->semester = $request->semester;
            $data->homeroom = $request->teacher;
            $data->save();

            // response success
            return redirect()->back()->with('alert', 'Kelas berhasil diubah.');
        } catch (\Throwable $th) {
            // response failure
            return redirect()->back()->with('alert-warning', 'Terjadi kesalahan pada sistem!.');
        }
    }

    public function destroy($id)
    {
        // fetch and delete classroom
        $classroom = ClassRoom::find($id);

        // get exam
        $exam = Exam::where('class_room_id', $classroom->id)->get();

        // fetch and delete exam
        if ($exam) {
            for ($i=0; $i < $exam->count(); $i++) {
                $exam_delete = Exam::find($exam[$i]->id);

                // get exam question
                $examQuestion = ExamQuestion::where('exam_id', $exam[$i]->id)->get();

                if ($examQuestion) {
                    for ($i=0; $i < $examQuestion->count(); $i++) {
                        $examQuestion_delete = ExamQuestion::find($examQuestion[$i]->id);
                        $examQuestion_delete->delete(); // exam question deleted
                    }
                }

                // exam deleted

                $exam_delete->delete();
            }
        }

        // get student
        $student = Student::where('class_room_id', $classroom->id)->get();

        if ($student) {
            for ($i=0; $i < $student->count(); $i++) {
                $student_delete = Student::find($student[$i]->id);

                // fetch user as student and delete
                $user_delete = User::find($student[$i]->user_id);

                if ($user_delete) {
                    // student deleted
                    $student_delete->delete();
                    // than delete user
                    $user_delete->delete();
                }
            }
        }

        // classroom deleted
        $classroom->delete();

        // response
        return redirect()->back()->with('alert','Data Kelas berhasil dihapus.');
    }
}
