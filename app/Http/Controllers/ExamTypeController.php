<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExamType;

class ExamTypeController extends Controller
{
    // page Jenis Ujian
    public function index()
    {
        $data = ExamType::orderBy('name', 'desc')->get();
        return view('page.exam-type', compact('data'));
    }

    // new Jenis Ujian
    public function store(Request $request)
    {
        try {
            $check = ExamType::where('name', $request->name)->first();
            if ($check) {
                return redirect()->back()->with('alert-warning', 'Jenis Ujian telah tersedia, coba dengan nama lain.');
            }

            // insert data
            $data = new ExamType();
            $data->name = $request->name;
            $data->save();

            // response success
            return redirect()->back()->with('alert', 'Jenis Ujian berhasil dibuat.');
        } catch (\Throwable $th) {
            // response failure
            return redirect()->back()->with('alert-warning', 'Terjadi kesalahan pada sistem!.');
        }
    }

    // update Jenis Ujian
    public function update(Request $request, $id)
    {
        try {
            $check = ExamType::where('name', $request->name)->first();
            if ($check) {
                return redirect()->back()->with('alert-warning', 'Jenis Ujian telah tersedia, coba dengan nama lain.');
            }

            // insert data
            $data = ExamType::find($id);
            $data->name = $request->name;
            $data->save();

            // response success
            return redirect()->back()->with('alert', 'Jenis Ujian berhasil diubah.');
        } catch (\Throwable $th) {
            // response failure
            return redirect()->back()->with('alert-warning', 'Terjadi kesalahan pada sistem!.');
        }
    }
}
