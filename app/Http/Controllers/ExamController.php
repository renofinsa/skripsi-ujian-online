<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exam;
use App\ExamType;
use App\ClassRoom;
use App\ExamAnswer;
use Auth;

class ExamController extends Controller
{
    public function index()
    {
        // get classroom
        $classroom = ClassRoom::select('id')->where('homeroom', Auth::user()->id)->get();

        // fetch data
        $list = [];
        foreach( $classroom as $item) {
            array_push($list, $item->id);
        }

        // get exam list
        $data = Exam::whereIn('class_room_id', $list)->get();

        // return
        return view('page.manage-exam', compact('data'));
    }

    public function create()
    {
        $classroom = ClassRoom::where('homeroom', Auth::user()->id)->get();
        $examtype = ExamType::all();
        return view('page.manage-exam-create', compact('classroom', 'examtype'));
    }

    public function store(Request $request)
    {
        // validate payload
        $get = $request->validate([
            'title' => ['required'],
            'exam_type_id' => ['required'],
            'class_room_id' => ['required'],
            'duration' => ['required'],
            'due_date' => ['required'],
            'notes' => ['required']
        ]);

        // insert
        $data = new Exam;
        $data->title = $get['title'];
        $data->exam_type_id = $get['exam_type_id'];
        $data->class_room_id = $get['class_room_id'];
        $data->duration = $get['duration'];
        $data->due_date = $get['due_date'];
        $data->notes = $get['notes'];
        $data->save();

        return redirect(route('teacher-exam'))->with('alert','Data ujian berhasil ditambah.');
    }

    public function edit($id)
    {
        $classroom = ClassRoom::where('homeroom', Auth::user()->id)->get();
        $examtype = ExamType::all();
        $data = Exam::find($id);
        return view('page.manage-exam-update', compact('data', 'classroom', 'examtype'));
    }

    public function update(Request $request, $id)
    {
        // validate payload
        $get = $request->validate([
            'title' => ['required'],
            'exam_type_id' => ['required'],
            'class_room_id' => ['required'],
            'duration' => ['required'],
            'due_date' => ['required'],
            'notes' => ['required']
        ]);

        // insert
        $data = Exam::find($id);
        $data->title = $get['title'];
        $data->exam_type_id = $get['exam_type_id'];
        $data->class_room_id = $get['class_room_id'];
        $data->duration = $get['duration'];
        $data->due_date = $get['due_date'];
        $data->notes = $get['notes'];
        $data->save();

        return redirect(route('teacher-exam'))->with('alert','Data ujian berhasil diubah.');
    }

    public function destroy($id)
    {
        //
    }

    public function start($id)
    {
        $data = Exam::find($id);
        return view('page.exam-start', compact('data', 'id'));
    }

    public function end(Request $request, $exam_id)
    {
        $pilihan = $request->get('pilihan');
        $id_soal = $request->get('id');
        $jawabanbenar = $request->get('jawabanbenar');

        $score=0;
	    $benar=0;
	    $salah=0;
	    $kosong=0;
        $i = 0;
        $nomor=$id_soal[$i];
	    //id nomor soal

        for ($i=0;$i<10;$i++)
        {
            //id nomor soal
            $nomor = $id_soal[$i];

            //jika user tidak memilih jawaban
            if ( empty($pilihan[$nomor]) ) {
              $kosong++;
            } else {
              //jawaban dari user
              $jawaban=$pilihan[$nomor];
              $jawabanbenararray=$jawabanbenar[$nomor];

              if($jawaban == $jawabanbenararray){
                //jika jawaban cocok (benar)
                $benar++;
              }else{
                //jika salah
                $salah++;
              }
            }
            $score = $benar*10;
        }

        // insert answer score
        $payload = array(
            'exam_id' => $exam_id,
            'student_id' => Auth::user()->id,
            'score' => ($score),
            'correct' => ($benar),
            'failure' => ($salah),
            'empty' => ($kosong),
            'status' => 1
        );
        ExamAnswer::create($payload);

        // return
        return redirect()->route('student');
    }
}
