<?php

namespace App\Exports;

use App\ExamAnswer;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromArray;

class scoreExport implements FromArray, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $id;

    function __construct($id) {
            $this->id = $id;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array
    {
        $result = [];
        $data = ExamAnswer::where('exam_id', $this->id)->get();
        foreach ($data as $item) {
            $payload = array(
                'nama_siswa' => $item->student->name,
                'mata_pelajaran' => $item->exam->title,
                'jenis_ujian' => $item->exam->type->name,
                'nilai' => $item->score == 0 ? "0" : $item->score,
                'tanggal_ujian' => date('d-m-Y', strtotime($item->created_at)),
            );
            array_push($result, $payload);
        }

        return $result;
    }

    public function headings(): array
    {
        return [
            'Nama Siswa',
            'Mata Pelajaran',
            'Jenis Ujian',
            'Nilai',
            'Tanggal Ujian',
        ];
    }
}
