<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    public function type(){
        return $this->belongsTo(ExamType::class,'exam_type_id');
    }

    public function classroom(){
        return $this->belongsTo(ClassRoom::class,'class_room_id');
    }

    public function question(){
        return $this->hasMany(ExamQuestion::class, 'exam_id');
    }

    public function answer(){
        return $this->hasOne(ExamAnswer::class, 'exam_id');
    }

    public function student_list(){
        return $this->hasMany(ExamAnswer::class, 'exam_id');
    }
}
