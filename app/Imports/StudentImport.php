<?php

namespace App\Imports;

use App\User;
use App\Student;
use App\ClassRoom;
use Maatwebsite\Excel\Concerns\ToModel;

class StudentImport implements ToModel
{
    public function model(array $row)
    {
        // check class with semester
        $classroom = ClassRoom::where([['name', $row[12]],['semester', $row[11]]])->first();

        // insert new user student
        $data = new User();
        $data->name = $row[0];
        $data->username = $row[1];
        $data->password = bcrypt('123123');
        $data->gender = $row[2];
        $data->religion = $row[3];
        $data->birthplace = $row[4];
        $data->birthday = $row[5];
        $data->address = $row[6];
        $data->phone_number = $row[7];
        $data->photo_profile = NULL;
        $data->role = 3;
        $data->save();

        // insert new detail student
        $student = new Student();
        $student->user_id = $data->id;
        $student->class_room_id = $classroom->id;
        $student->father_name = $row[8];
        $student->mother_name = $row[9];
        $student->parent_phone_number = $row[10];
        $student->save();
    }
}
