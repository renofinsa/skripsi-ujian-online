<?php

namespace App\Imports;

use App\User;
use Maatwebsite\Excel\Concerns\ToModel;

class TeacherImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // insert new user student
        $data = new User();
        $data->name = $row[0];
        $data->username = $row[1];
        $data->password = bcrypt('123123');
        $data->gender = $row[2];
        $data->religion = $row[3];
        $data->birthplace = $row[4];
        $data->birthday = $row[5];
        $data->address = $row[6];
        $data->phone_number = $row[7];
        $data->photo_profile = NULL;
        $data->role = 2;
        $data->save();
    }
}
