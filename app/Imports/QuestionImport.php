<?php

namespace App\Imports;

use App\ExamQuestion;
use Maatwebsite\Excel\Concerns\ToModel;

class QuestionImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new ExamQuestion([
            "id" => NULL,
            "exam_id" => $row[0],
            "question" => $row[1],
            "answer_a" => $row[2],
            "answer_b" => $row[3],
            "answer_c" => $row[4],
            "answer_d" => $row[5],
            "answer_e" => $row[6],
            "answer_correct" => $row[7]
        ]);
    }
}
